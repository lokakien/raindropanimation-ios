//
//  ViewController.swift
//  Test
//
//  Created by Matthew on 12/07/2016.
//  Copyright © 2016 Matthew. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var button: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func buttonHandler(sender: AnyObject?) {

        for _ in 0..<1000 {
            self.fallingSmiley()
        }
    }

    @IBAction func goToNextVC(sender: AnyObject) {
        let nextVC = self.storyboard?.instantiateViewControllerWithIdentifier("NextViewController") as! NextViewController
        self.navigationController?.pushViewController(nextVC, animated: true)

    }

    func fallingSmiley() {

        let randX = CGFloat(arc4random())%self.view.frame.width
        let rect = CGRect(x: randX, y: 0, width: 60, height: 60)
        let label = UILabel(frame: rect)
        label.text = "💧"

        let scaleRandom = CGFloat(arc4random_uniform(UInt32(8)+2))/4
        label.transform = CGAffineTransformScale(CGAffineTransformIdentity, scaleRandom, scaleRandom)
        self.view.addSubview(label)
        let randomDelay = Double(arc4random()%1000)/10

        let randomTime = Double(5 - scaleRandom)
        UIView.animateWithDuration(randomTime, delay: randomDelay , options: .CurveLinear ,animations: {
            label.frame = CGRect(x: label.frame.origin.x,
                                y: self.view.frame.height,
                                width: label.frame.width,
                                height: label.frame.height)
            self.view.layoutIfNeeded()

            }, completion: { (Bool: Bool) -> Void
                in
                if Bool {
                    label.removeFromSuperview()
                    self.fallingSmiley()
                    print ("finished")
                } else {
                    print ("not finished")
                }
        })

    }
}



